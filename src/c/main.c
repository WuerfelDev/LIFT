#include "pebble.h"

#define PANELH 50

// persistent settings key
#define SETTINGS_KEY 1

static Window *window;
static TextLayer *timeLayer;

typedef struct ClaySettings {
  GColor bar_color;
  GColor background_color;
  bool vibrate_disconnect;
} __attribute__((__packed__)) ClaySettings;
ClaySettings settings;

/*
-- TODO --
-show icon after disconnect for some time (not important)
-animations (quickview & full hour)
    - what about hour-change while quickview is on!
    - how does it work when the location is overwritten by handle_tick?
*/

GColor text_color_by_luminance(GColor color) {
  uint8_t r = color.r * 85;
  uint8_t g = color.g * 85;
  uint8_t b = color.b * 85;
  uint32_t luminance = (r << 1) + (g << 2) + g + b;
  return (luminance > (128 << 3)) ? GColorBlack : GColorWhite;
}

static void handle_tick(struct tm *currentTime, TimeUnits units_changed) {
  Layer *timeLayer_new = text_layer_get_layer(timeLayer);
  GRect bounds = layer_get_bounds(timeLayer_new);

  static char timeText[] = "LI:FT"; // 00:00
  strftime(timeText, sizeof(timeText), clock_is_24h_style() ? "%H:%M" : "%I:%M",
           currentTime);
  text_layer_set_text(timeLayer, timeText);
  bounds.origin.y =
      currentTime->tm_min *
      (layer_get_unobstructed_bounds(window_get_root_layer(window)).size.h -
       PANELH) /
      60;

  // bounds.origin.y =
  // currentTime->tm_min*(layer_get_bounds(window_get_root_layer(window)).size.h-PANELH)/60;
  layer_set_frame(timeLayer_new, bounds);
  layer_mark_dirty(timeLayer_new);
}

static void unobstructed_will_change(GRect final_unobstructed_screen_area,
                                     void *context) {
  time_t now = time(NULL);
  struct tm *tick_time = localtime(&now);
  handle_tick(tick_time, MINUTE_UNIT);
}

static void unobstructed_did_change(void *context) {
  time_t now = time(NULL);
  struct tm *tick_time = localtime(&now);
  handle_tick(tick_time, MINUTE_UNIT);
}
static void unobstructed_changing(AnimationProgress progress, void *context) {
  time_t now = time(NULL);
  struct tm *tick_time = localtime(&now);
  handle_tick(tick_time, MINUTE_UNIT);
}

static void bluetooth_callback(bool connected) {
  if (!connected && settings.vibrate_disconnect) {
    vibes_double_pulse();
  }
}

// Handle receiving clay settings
static void inbox_received_handler(DictionaryIterator *iter, void *context) {
  Tuple *tuple;
  if ((tuple = dict_find(iter, MESSAGE_KEY_BarColor))) {
    settings.bar_color = GColorFromHEX(tuple->value->int32);
  }

#if PBL_COLOR
  if ((tuple = dict_find(iter, MESSAGE_KEY_BackgroundColor))) {
    settings.background_color = GColorFromHEX(tuple->value->int32);
  }
#else
  settings.background_color = text_color_by_luminance(settings.bar_color);
#endif

  if ((tuple = dict_find(iter, MESSAGE_KEY_BarColor))) {
    settings.vibrate_disconnect = tuple->value->int8;
  }

  // Save the new settings to persistent storage
  persist_write_data(SETTINGS_KEY, &settings, sizeof(settings));
  // Update the display based on new settings
  text_layer_set_background_color(timeLayer, settings.bar_color);
  text_layer_set_text_color(timeLayer,
                            text_color_by_luminance(settings.bar_color));
  window_set_background_color(window, settings.background_color);
  layer_mark_dirty(text_layer_get_layer(timeLayer));
}

static void main_window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  timeLayer = text_layer_create(
      GRect(0, 0, layer_get_bounds(window_layer).size.w, PANELH));
  text_layer_set_text_color(timeLayer,
                            text_color_by_luminance(settings.bar_color));
  text_layer_set_background_color(timeLayer, settings.bar_color);
  text_layer_set_font(timeLayer,
                      fonts_get_system_font(FONT_KEY_BITHAM_42_BOLD));
  text_layer_set_text_alignment(timeLayer, GTextAlignmentCenter);
  layer_add_child(window_layer, text_layer_get_layer(timeLayer));

  // Ensures time is displayed immediately (will break if NULL tick event
  // accessed). (This is why it's a good idea to have a separate routine to do
  // the update itself.)
  time_t now = time(NULL);
  struct tm *tick_time = localtime(&now);
  handle_tick(tick_time, MINUTE_UNIT);
  tick_timer_service_subscribe(MINUTE_UNIT, handle_tick);

  UnobstructedAreaHandlers handlers = {.will_change = unobstructed_will_change,
                                       .change = unobstructed_changing,
                                       .did_change = unobstructed_did_change};
  unobstructed_area_service_subscribe(handlers, NULL);
}

static void main_window_unload(Window *window) {
  connection_service_unsubscribe();
  unobstructed_area_service_unsubscribe();
  text_layer_destroy(timeLayer);
}

static void handlers_init(void) {
  // default settings
  settings.bar_color = PBL_IF_COLOR_ELSE(GColorRed, GColorWhite);
  settings.background_color = GColorBlack;
  settings.vibrate_disconnect = true;
  // Read settings from persistent storage, if they exist
  persist_read_data(SETTINGS_KEY, &settings, sizeof(settings));

  // Open AppMessage connection
  // https://developer.rebble.io/developer.pebble.com/guides/communication/using-pebblekit-js/index.html#type-conversion
  app_message_register_inbox_received(inbox_received_handler);
  uint32_t recv_buffer_size = dict_calc_buffer_size(
      3, sizeof(int32_t), sizeof(int32_t), sizeof(int16_t));
  app_message_open(recv_buffer_size, 0);

  connection_service_subscribe((ConnectionHandlers){
      .pebble_app_connection_handler = bluetooth_callback});
}
static void handlers_deinit(void) {}

static void window_init(void) {
  window = window_create();
  window_set_window_handlers(window, (WindowHandlers){
                                         .load = main_window_load,
                                         .unload = main_window_unload,
                                     });

  window_set_background_color(window, settings.background_color);
  window_stack_push(window, true); // Animated
}
static void window_deinit(void) {
  if (window) {
    window_destroy(window);
  }
}

int main(void) {
  handlers_init();
  window_init();
  app_event_loop();
  handlers_deinit();
  window_deinit();
}
